import java.util.Scanner;

public class Main {

    public static void main(String args[]) throws Exception {

        BallsAndHats ballsAndHats = new BallsAndHats();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ingrese la posicion inicial de la bolita.");
        Integer firstPlace = scanner.nextInt();
        System.out.println("Ingrese la cantidad de movimientos realizados por el vaso con la bolita.");
        Integer swaps = scanner.nextInt();
        System.out.println("\nLa bolita deberia estar en la posicion: "+ballsAndHats.loadData(firstPlace,swaps));

    }
}
