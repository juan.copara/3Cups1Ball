import Model.Position;

import java.util.*;

public class BallsAndHats {

    private NavigableMap<Integer, Position> table = new TreeMap<>();

    private final Integer hats = 3;

    private final String codeFirstPosition ="o";

    private final String codeEmptyPosition =".";

    private void initTable(){
        table.clear();
        for(int i= 0 ; i < hats; i++ ){
            table.put(i,new Position());
        }
        
    }

    public Integer loadData(Integer firstPosition, Integer swaps) throws Exception {

        initTable();
        validateData(firstPosition, swaps);
        Position position = table.get(firstPosition);
        position.setBfirst(true);
        table.put(firstPosition,position);

        printTable();
        
        return predictPlace(firstPosition, swaps);
    }

    private Integer predictPlace(Integer firstPosition, Integer swaps){

        Integer aux = 0;

        boolean bmovement = false; //false left true right

        Integer position = firstPosition;
        while (swaps > aux){

            if(table.containsKey(position-1) && table.containsKey(position+1)){
                bmovement = false;
            }

            if(bmovement){

                if(table.containsKey(position+1)){
                    position++;
                }else{
                    bmovement = false;
                    position--;
                }

            }else{

                if(table.containsKey(position-1)){
                    position--;
                }else{
                    bmovement = true;
                    position++;
                }

            }
            aux++;
        }

        return position;
    }

    private void validateData(Integer firstPosition, Integer swaps){
        if(firstPosition > hats || firstPosition < 0  || swaps < 0) {
            Exception e = new Exception("Datos invalidos");
            e.printStackTrace();
        }

    }
    
    
    private void printTable(){
        for (Map.Entry<Integer, Position> entry : table.entrySet() ) {
            if(entry.getValue().isBfirst()){
                System.out.print(codeFirstPosition);
            }
            else {
                System.out.print(codeEmptyPosition);
            }
        }
    }
}
