package Model;

public class Position {

    private boolean bTemporal = false;

    private boolean bfirst = false;

    private Integer nposition;

    public boolean isbTemporal() {
        return bTemporal;
    }

    public void setbTemporal(boolean bTemporal) {
        this.bTemporal = bTemporal;
    }

    public boolean isBfirst() {
        return bfirst;
    }

    public void setBfirst(boolean bfirst) {
        this.bfirst = bfirst;
    }

    public Integer getNposition() {
        return nposition;
    }

    public void setNposition(Integer nposition) {
        this.nposition = nposition;
    }
}
